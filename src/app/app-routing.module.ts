import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import {LoginComponent} from "./shared/components/login/login.component";
import {RegisterComponent} from "./shared/components/register/register.component";

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'Login' },
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: { title: 'Register' },
  },
  // {
  //   path: '',
  //   //canActivate: [AuthGuard],
  //   //canLoad: [AuthGuard],
  //   children: [
  //     {
  //       loadChildren: () =>
  //         import('./features/admin/admin.module').then(
  //           (m) => m.AdminModule
  //         ),
  //     },
  //   ],
  // },
  { path: '**', redirectTo: 'login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
