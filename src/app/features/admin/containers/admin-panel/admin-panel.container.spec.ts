import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPanelContainer } from './admin-panel.container';

describe('AdminPanelComponent', () => {
  let component: AdminPanelContainer;
  let fixture: ComponentFixture<AdminPanelContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminPanelContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPanelContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
