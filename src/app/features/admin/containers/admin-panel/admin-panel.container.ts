import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.container.html',
  styleUrls: ['./admin-panel.container.scss']
})
export class AdminPanelContainer implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
