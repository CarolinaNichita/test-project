import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from "../../shared/shared.module";

// Components
import {AdminPanelContainer} from './containers/admin-panel/admin-panel.container';
import {UserListsComponent} from './components/user-lists/user-lists.component';

@NgModule({
  declarations: [
    AdminPanelContainer,
    UserListsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
  ]
})
export class AdminModule {
}
