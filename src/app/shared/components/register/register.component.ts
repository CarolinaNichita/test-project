import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public countries: string[] = ['Moldova', 'Romania', 'Ucraina', 'Switzerland', 'Poland', 'Netherland'];
  // @ts-ignore
  public form: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit():void {
    this.initForm();
  }

  private initForm():void {
    this.form = this.fb.group ({
      fullName: ['Carolina Nichita', Validators.required],
      gender: ['female', Validators.required],
      country: ['Moldova', Validators.required],
      birthday: [new Date(1989, 2, 2), Validators.required],
      email: ['email@gmail.com', [Validators.required, Validators.email]],
      password: ['1111111', [Validators.required, Validators.minLength(7)]],
      agree: [false, Validators.requiredTrue],
      phones: ['37379097000', Validators.required],
    })
  }

  public submitForm(): void {
    console.log(this.form.value);
  }

}
